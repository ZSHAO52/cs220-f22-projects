# Project 5 (P5): Investigating Hurricane Data


## Corrections and clarifications

* 10/5/2022 4:00 PM - `p5.ipynb` replaced; bug in Q10 (see [here](https://piazza.com/class/l7f7vr5x63n7l1/post/279)) that caused the notebook to not recognize the correct answer (which is `1000000`) is now fixed. If you have already started working on the old `p5.ipynb`, you may continue working on your old notebook. You can ignore this error - Gradescope will grade you correctly if your answer is `1000000`. Otherwise, download the new notebook.
* 10/7/2022 12:45 PM - Section `IMPORTANT Submission instructions` of the `README` updated. **Please read the instructions carefully**.

**Find any issues?** Report to us:

- Iffat Nafisa <nafisa@wisc.edu>
- Sahana Prasad <sprasad25@wisc.edu>

## Instructions:

This project will focus on **loops** and **strings**. To start, download `p5.ipynb`, `project.py`, `p5_test.py` and `hurricanes.csv` . For each project, we will have you download a new `p5_test.py` - please make sure to download the correct files for the current project.

**Note:** Please go through [lab-p5](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f22-projects/-/tree/main/lab-p5) before you start the project. The lab contains some very important information that will be necessary for you to finish the project.

You will work on `p5.ipynb` and hand it in. You should follow the provided directions for each question. Questions have **specific** directions on what **to do** and what **not to do**. 

After you've downloaded the file to your `p5` directory, open a terminal window and use `cd` to navigate to that directory. To make sure you're in the correct directory in the terminal, type `pwd`. To make sure you've downloaded the notebook file, type `ls` to ensure that `p5.ipynb`, `project.py`, `p5_test.py`, and `hurricanes.csv` are listed. Then run the command `jupyter notebook` to start Jupyter, and get started on the project!

**IMPORTANT**: You should **NOT** terminate/close the session where you run the above command. If you need to use any other Terminal/PowerShell commands, open a new window instead. Keep constantly saving your notebook file, by either clicking the "Save and Checkpoint" button (floppy disk) or using the appropriate keyboard shortcut.

------------------------------

## IMPORTANT Submission instructions:
- Review the [Grading Rubric](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f22-projects/-/tree/main/p5/rubric.md), to ensure that you don't lose points during code review.
- You must **save your notebook file** before you run the cell containing **export**.
- Login to [Gradescope](https://www.gradescope.com/) and upload the zip file into the P5 assignment.
- It is **your responsibility** to make sure that your project clears auto-grader tests on the Gradescope test system. Otter test results should be available in a few minutes after your submission. You should be able to see both PASS / FAIL results for the 20 test cases and your total score, which is accessible via Gradescope Dashboard (as in the image below):
       <img src="images/autograder_success.png" width="400">
