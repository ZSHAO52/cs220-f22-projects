# Project 4 (P4) grading rubric 

## Code reviews

- A TA / grader will be reviewing your code after the deadline.
- They will make deductions based on the Rubric provided below.
- To ensure that you don`t lose any points in code review, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.
- Incorrect function logic loses points in manual code review - for example, incorrectly ordered branches of a conditional or incorrectly ordered two different conditionals (ex: battle function definition)

## Rubric

### General guidelines:

- Did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. This deduction will become stricter for future projects. (-1)
- Used concepts not covered in class yet (-1)

### Question specific guidelines:

- Q1 deductions
	- `damage_zacian_mewtwo` answer is hardcoded (-5)
	- `damage` function logic is incorrect (-3)
	- `Zacian` and `Mewtwo` are not passed as arguments to `damage` function (-2)

- Q2 deductions
	- `damage_slowbro_slowpoke` answer is hardcoded (-5)
	- `Slowbro` and `Slowpoke` are not passed as arguments to `damage` function (-2)

- Q3 deductions
	- `bonus_water_charmander` answer is hardcoded (-5)
	- `type_bonus` function logic is incorrect (-3)
	- Appropriate function(s) from project.py are not used in `type_bonus` function (-2)
	- `Water` and `Charmander` are not passed as arguments to `type_bonus` function (-2)

- Q4 deductions
	- `bonus_poison_roserade` answer is hardcoded (-5)
	- `Poison` and `Roserade` are not passed as arguments to `type_bonus` function (-2)

- Q5 deductions
	- `eff_damage_garchomp_gligar` answer is hardcoded (-5)
	- `effective_damage`/`get_num_types` function logic is incorrect (-3)
	- Appropriate functions (like `get_num_types`) are not used in `effective_damage` function (-2)
	- `Garchomp` and `Gligar` are not passed as arguments to `effective_damage` function (-2)

- Q6 deductions
	- `eff_damage_sceptile_umbreon` answer is hardcoded (-5)
	- `Sceptile` and `Umbreon` are not passed as arguments to `effective_damage` function (-2)

- Q7 deductions
	- `eff_damage_infernape_starly` answer is hardcoded (-5)
	- `Infernape` and `Starly` are not passed as arguments to `effective_damage` function (-2)

- Q8 deductions
	- `hits_bellsprout_mewtwo` answer is hardcoded (-5)
	- `num_hits` function logic is incorrect (-3)
	- Appropriate function(s) from project.py are not used in `num_hits` function (-2)
	- import math is not written at top of the notebook (-1)
	- `Bellsprout` and `Mewtwo` are not passed as arguments to `num_hits` function (-2)

- Q9 deductions
	- `hits_metapod_torterra` answer is hardcoded (-5)
	- `Metapod` and `Torterra` are not passed as arguments to `num_hits` function (-2)

- Q10 deductions
	- `battle_bulbasaur_flareon` answer is hardcoded (-5)
	- `battle` function logic is incorrect (-3)
	- Appropriate function(s) from project.py are not used in `battle` function (-2)
	- `Bulbasaur` and `Flareon` are not passed as arguments to `battle` function (-2)

- Q11 deductions
	- `battle_blastoise_charizard` answer is hardcoded (-5)
	- `Blastoise` and `Charizard` are not passed as arguments to `battle` function (-2)

- Q12 deductions
	- `battle_pidgey_starly` answer is hardcoded (-5)
	- `battle` function redefined (-3)
	- `Pidgey` and `Starly` are not passed as arguments to `battle` function (-2)

- Q13 deductions
	- `battle_mudkip_gulpin` answer is hardcoded (-5)
	- `Mudkip` and `Gulpin` are not passed as arguments to `battle` function (-2)

- Q14 deductions
	- `battle_rayquaza_pikachu` answer is hardcoded (-5)
	- `battle` function redefined (-3)
	- `Rayquaza` and `Pikachu` are not passed as arguments to `battle` function (-2)

- Q15 deductions
	- `battle_lucario_jigglypuff` answer is hardcoded (-5)
	- `Lucario` and `Jigglypuff` are not passed as arguments to `battle` function (-2)

- Q16 deductions
	- `battle_mew_celebi` answer is hardcoded (-5)
	- `Mew` and `Celebi` are not passed as arguments to `battle` function (-2)

- Q17 deductions
	- `friendship_pikachu_squirtle` answer is hardcoded (-5)
	- `friendship_score` function logic is incorrect (-3)
	- Appropriate function(s) from project.py are not used in `friendship_score` function (-2)
	- `Pikachu` and `Squirtle` are not passed as arguments to `friendship_score` function (-2)

- Q18 deductions
	- `friendship_growlithe_umbreon` answer is hardcoded (-5)
	- `Growlithe` and `Umbreon` are not passed as arguments to `friendship_score` function (-2)

- Q19 deductions
	- `friendship_nidoking_nidoqueen` answer is hardcoded (-5)
	- `Nidoking` and `Nidoqueen` are not passed as arguments to `friendship_score` function (-2)

- Q20 deductions
	- `friendship_snivy_snivy` answer is hardcoded (-5)
	- `Snivy` and `Snivy` are not passed as arguments to `friendship_score` function (-2)
